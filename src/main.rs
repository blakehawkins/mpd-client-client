#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate serde_derive;

use std::collections::{BTreeMap, VecDeque};
use std::default;
use std::path::PathBuf;
use std::process::Stdio;
use std::sync::{atomic::AtomicUsize, Arc, Mutex};

use futures_util::{future::FusedFuture, FutureExt};

use actix_web::web::{get, post, Json, Path};
use actix_web::{App, HttpResponse, HttpServer, Responder};
use askama::Template;
use regex::Regex;
use structopt::StructOpt;
use tokio::process::Command;

#[derive(StructOpt, Debug)]
#[structopt(name = "http-media-wrapper")]
struct Opt {
    #[structopt(short = "p", long = "port", default_value = "8080")]
    port: u32,

    ///
    /// Script overrides.
    ///
    #[structopt(
        long = "play-script",
        default_value = "scripts/play.sh",
        parse(from_os_str)
    )]
    play_script: PathBuf,

    #[structopt(
        short = "k",
        long = "kill-script",
        default_value = "scripts/stop.sh",
        parse(from_os_str)
    )]
    kill_script: PathBuf,

    #[structopt(
        short = "w",
        long = "pause-script",
        default_value = "scripts/pause.sh",
        parse(from_os_str)
    )]
    pause_script: PathBuf,

    #[structopt(
        short = "n",
        long = "skip-script",
        default_value = "scripts/skip.sh",
        parse(from_os_str)
    )]
    skip_script: PathBuf,

    #[structopt(
        short = "q",
        long = "queue-script",
        default_value = "scripts/queue.sh",
        parse(from_os_str)
    )]
    queue_script: PathBuf,

    #[structopt(
        short = "l",
        long = "list-script",
        default_value = "scripts/list.sh",
        parse(from_os_str)
    )]
    list_script: PathBuf,

    #[structopt(
        short = "v",
        long = "volume-down-script",
        default_value = "scripts/volume_down.sh",
        parse(from_os_str)
    )]
    volume_down_script: PathBuf,

    #[structopt(
        short = "V",
        long = "volume-up-script",
        default_value = "scripts/volume_up.sh",
        parse(from_os_str)
    )]
    volume_up_script: PathBuf,

    #[structopt(
        short = "s",
        long = "seek-backwards-script",
        default_value = "scripts/seek_backwards.sh",
        parse(from_os_str)
    )]
    seek_backwards_script: PathBuf,

    #[structopt(
        short = "S",
        long = "seek-forwards-script",
        default_value = "scripts/seek_forwards.sh",
        parse(from_os_str)
    )]
    seek_forwards_script: PathBuf,

    #[structopt(
        short = "i",
        long = "info",
        default_value = "scripts/status.sh",
        parse(from_os_str)
    )]
    info_script: PathBuf,

    #[structopt(
        short = "r",
        long = "results",
        default_value = "scripts/live_results.sh",
        parse(from_os_str)
    )]
    results_script: PathBuf,

    #[structopt(
        short = "Q",
        long = "query",
        default_value = "scripts/query.py",
        parse(from_os_str)
    )]
    query_script: PathBuf,
}

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {}

#[derive(Template)]
#[template(path = "script_out.html")]
struct ScriptOutTemplate {
    lines: Vec<String>,
}

#[derive(Template)]
#[template(path = "snippet.html")]
struct SnippetTemplate {
    lines: Vec<String>,
}

#[derive(Template)]
#[template(path = "playlist.html")]
struct PlaylistTemplate {
    lines: Vec<String>,
}

mod filters {
    pub fn cut_m4a(s: &str) -> ::askama::Result<String> {
        Ok(s.replace(".m4a", ""))
    }

    // pub fn extension(s: &str) -> ::askama::Result<String> {
    //     Ok(s.split('.').last().expect("Couldn't split by '.'").into())
    // }

    /// Given a string with words separated by dots, drop the first two
    /// dot-separated segments.
    pub fn third_dotted_plus(s: &str) -> ::askama::Result<String> {
        Ok(s.split('.').skip(2).collect::<Vec<_>>().join("."))
    }

    pub fn underscore_to_space(s: &str) -> ::askama::Result<String> {
        Ok(s.replace("_", " "))
    }
}

async fn index(_info: Path<()>) -> impl Responder {
    let s = IndexTemplate {}.render().expect("Couldn't build index");

    HttpResponse::Ok().content_type("text/html").body(s)
}

fn _default_template(stdout: String) -> String {
    ScriptOutTemplate {
        lines: stdout.split('\n').map(|l| l.into()).collect(),
    }
    .render()
    .expect("Couldn't render script out template")
}

fn _snippet_template(stdout: String) -> String {
    SnippetTemplate {
        lines: stdout.split('\n').map(|l| l.into()).collect(),
    }
    .render()
    .expect("Couldn't render snippet template")
}

fn _script_out_str(script_name: String, args: Vec<String>) -> (String, std::process::Output) {
    let mut cmd = std::process::Command::new(format!("./scripts/{}", script_name));

    args.into_iter().for_each(|x| {
        cmd.arg(x);
    });

    let output = cmd
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .unwrap_or_else(|_| panic!("Failed to execute {}", script_name))
        .wait_with_output()
        .unwrap_or_else(|_| panic!("Failed to wait on {}", script_name));

    (String::from_utf8_lossy(&output.stdout).to_string(), output)
}

/// Generate html from a script output.  template_fn defaults to a closure that
/// uses ScriptOutTemplate.
async fn script_out(
    script_name: String,
    args: Vec<String>,
    verbose_logs: bool,
    template_fn: Option<&dyn Fn(String) -> String>,
) -> impl Responder {
    let (stdout, raw_output) = _script_out_str(script_name, args);

    let templated = template_fn.unwrap_or(&_default_template)(stdout.to_string());

    if verbose_logs {
        println!("Script out: {}", stdout);
    }

    match raw_output.status.code().unwrap_or(0) {
        0 => HttpResponse::Ok().content_type("text/html").body(templated),
        _ => HttpResponse::InternalServerError()
            .content_type("text/html")
            .body(raw_output.stderr),
    }
}

async fn clear(_info: Path<()>) -> impl Responder {
    script_out("clear.sh".into(), vec![], true, None).await
}

async fn status(_info: Path<()>) -> impl Responder {
    script_out(
        "status.sh".into(),
        vec![],
        false,
        Some(&|stdout: String| {
            SnippetTemplate {
                lines: stdout.split('\n').map(|l| l.into()).collect(),
            }
            .render()
            .expect("Couldn't render snippet template")
        }),
    )
    .await
}

#[derive(Serialize, Default)]
struct Status {
    title: Option<String>,
    playing: Option<bool>,
    queue_length: Option<usize>,
    elapsed: Option<String>,
    duration: Option<String>,
    complete_pct: Option<u16>,
    volume_pct: u16,
    repeat: bool,
    randomise: bool,
    single: bool,
    consume: bool,
}

#[derive(Serialize, Default)]
struct LiveResult {
    result: String,
}

#[derive(Serialize)]
struct QueueState(BTreeMap<usize, (String, String)>);

lazy_static! {
    static ref FILENAME_RE: Regex =
        Regex::new(r"/(.*/)*\d+\.[^\.]+\.(.+)\.m4a").expect("Couldn't make regular expression");
    static ref PLAYBACK_RE: Regex =
        Regex::new(r"(\[.*\])? *\#\d+/(\d+) +([\d:]+)/([\d:]+) \((\d+)%\)")
            .expect("Couldn't make regular expression");
    static ref BAR_RE: Regex = Regex::new(
        r"volume: ?(\d+)% +repeat: o(n|ff) +random: o(n|ff) +single: o(n|ff) +consume: o(n|ff) *"
    )
    .expect("Couldn't make regular expression");
    static ref POSTS: Mutex<
        VecDeque<(
            usize,
            String,
            Arc<dyn FusedFuture<Output = String> + Sync + Send>
        )>,
    > = Mutex::new(VecDeque::new());
    static ref POST_GENERATOR: AtomicUsize = AtomicUsize::new(0);
}

async fn status_json(_info: Path<()>) -> impl Responder {
    let mut status: Status = default::Default::default();
    let (stdout, _output) = _script_out_str("status.sh".into(), vec![]);

    stdout.lines().for_each(|l| {
        match l {
            l if FILENAME_RE.is_match(l) => {
                status.title = Some(
                    FILENAME_RE
                        .captures(l)
                        .unwrap()
                        .get(2)
                        .unwrap()
                        .as_str()
                        .replace("_", " "),
                )
            }
            l if PLAYBACK_RE.is_match(l) => {
                // status.elapsed = Some(l.into())
                let caps = PLAYBACK_RE.captures(l).unwrap();
                status.playing = match caps.get(1).unwrap().as_str() {
                    "[paused]" => Some(false),
                    "[playing]" => Some(true),
                    _ => None,
                };

                status.queue_length = Some(caps.get(2).unwrap().as_str().parse().unwrap());
                status.elapsed = Some(caps.get(3).unwrap().as_str().into());
                status.duration = Some(caps.get(4).unwrap().as_str().into());
                status.complete_pct = Some(caps.get(5).unwrap().as_str().parse().unwrap());
            }
            l if BAR_RE.is_match(l) => {
                let caps = BAR_RE.captures(l).unwrap();
                status.volume_pct = caps.get(1).unwrap().as_str().parse().unwrap();
                status.repeat = matches!(caps.get(2).unwrap().as_str(), "n");
                status.randomise = matches!(caps.get(3).unwrap().as_str(), "n");
                status.single = matches!(caps.get(4).unwrap().as_str(), "n");
                status.consume = matches!(caps.get(5).unwrap().as_str(), "n");
            }
            other => println!("Other pattern... {:?}", other),
        }
    });

    Json(status)
}

async fn live_results(info: Path<(String,)>) -> impl Responder {
    let mut results = vec![];
    let (stdout, _raw_output) = _script_out_str("live_results.sh".into(), vec![info.0.to_string()]);

    stdout.lines().for_each(|line| {
        results.push(LiveResult {
            result: line.into(),
        });
    });

    Json(results)
}

async fn query(query: Path<(String,)>) -> impl Responder {
    let (stdout, _raw_input) = _script_out_str("query.py".into(), vec![query.0.to_string()]);
    let parsed: Vec<serde_json::Value> = serde_json::from_str(&stdout).expect("bad json");

    Json(parsed)
}

async fn list(_info: Path<()>) -> impl Responder {
    script_out(
        "list.sh".into(),
        vec![],
        false,
        Some(&|stdout: String| {
            PlaylistTemplate {
                lines: stdout
                    .split('\n')
                    .map(|l| l.split('/').last().unwrap_or("").trim().to_string())
                    .filter(|ref x| !x.is_empty())
                    .collect(),
            }
            .render()
            .expect("Couldn't render snippet template")
        }),
    )
    .await
}

async fn pause(_info: Path<()>) -> impl Responder {
    script_out("pause.sh".into(), vec![], true, None).await
}

async fn play(_info: Path<()>) -> impl Responder {
    script_out("play.sh".into(), vec![], true, None).await
}

async fn stop(_info: Path<()>) -> impl Responder {
    script_out("stop.sh".into(), vec![], true, None).await
}

async fn volume_up(_info: Path<()>) -> impl Responder {
    script_out("volume_up.sh".into(), vec![], true, None).await
}

async fn volume_down(_info: Path<()>) -> impl Responder {
    script_out("volume_down.sh".into(), vec![], true, None).await
}

async fn seek_backwards(_info: Path<()>) -> impl Responder {
    script_out("seek_backwards.sh".into(), vec![], true, None).await
}

async fn seek_forwards(_info: Path<()>) -> impl Responder {
    script_out("seek_forwards.sh".into(), vec![], true, None).await
}

async fn skip(_info: Path<()>) -> impl Responder {
    script_out("skip.sh".into(), vec![], true, None).await
}

async fn queue(info: Path<(String,)>) -> impl Responder {
    script_out(
        "queue.sh".into(),
        vec![info.0.to_string()],
        true,
        Some(&_snippet_template),
    )
    .await
}

async fn queue_v2(info: Path<(String,)>) -> impl Responder {
    let id = POST_GENERATOR.fetch_add(1, std::sync::atomic::Ordering::SeqCst);

    let fut = Command::new("./scripts/queue.sh")
        .arg(&info.0)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .output()
        .map(|v| String::from_utf8_lossy(&v.unwrap().stdout).into());

    POSTS
        .lock()
        .unwrap()
        .push_back((id, info.0.clone(), Arc::new(fut)));

    Json(id)
}

async fn inspect_queue(_: Path<()>) -> impl Responder {
    Json(QueueState({
        let mut map = BTreeMap::new();

        POSTS.lock().unwrap().iter().for_each(|(idx, hash, fut)| {
            map.insert(*idx, (hash.into(), queue_status(fut)));
        });

        map
    }))
}

fn queue_status<T: FusedFuture<Output = String> + Sync + Send + ?Sized>(fut: &Arc<T>) -> String {
    if fut.is_terminated() {
        "Done".into()
    } else {
        "In progress...".into()
    }
}

async fn insert(info: Path<(String,)>) -> impl Responder {
    script_out(
        "insert.sh".into(),
        vec![info.0.to_string()],
        true,
        Some(&_snippet_template),
    )
    .await
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();

    HttpServer::new(move || {
        App::new()
            .route("/", get().to(index))
            .route("/stop", get().to(stop))
            .route("/play", get().to(play))
            .route("/pause", get().to(pause))
            .route("/skip", get().to(skip))
            .route("/status", get().to(status))
            .route("/status_json", get().to(status_json))
            .route("/queue/{id}", get().to(queue))
            .route("/v2/queue/{id}", post().to(queue_v2))
            .route("/v2/inspect_queue", get().to(inspect_queue))
            .route("/insert/{id}", get().to(insert))
            .route("/clear", get().to(clear))
            .route("/list", get().to(list))
            .route("/volume_up", get().to(volume_up))
            .route("/volume_down", get().to(volume_down))
            .route("/seek_backwards", get().to(seek_backwards))
            .route("/seek_forwards", get().to(seek_forwards))
            .route("/live_results/{query}", get().to(live_results))
            .route("/query/{query}", get().to(query))
    })
    .bind(&format!("0.0.0.0:{}", opt.port))?
    .run()
    .await
}

#[test]
fn test_filename_re() {
    // Regex::new(r"/(.*/)*\d+\.[^\.]+\.(.+)\.m4a")
    let test_str = r"/home/pi/media/1532981725.REwNxxd1qgI.03_GoGo_Penguin_-_Ka.m4a";
    assert!(FILENAME_RE.is_match(test_str));
    assert_eq!(
        FILENAME_RE
            .captures(test_str)
            .expect("Failed to capture")
            .get(2)
            .expect("Failed to capture")
            .as_str(),
        "03_GoGo_Penguin_-_Ka"
    );
}

#[test]
fn test_playback_re() {
    let test_str = r"[playing] #1/3   0:39/3:47 (17%)";
    assert!(PLAYBACK_RE.is_match(r"[paused]  #1/82   0:20/3:19 (10%)"));
    assert!(PLAYBACK_RE.is_match(test_str));
    assert_eq!(
        PLAYBACK_RE
            .captures(test_str)
            .expect("Failed to capture")
            .get(1)
            .expect("Failed to capture")
            .as_str(),
        "[playing]"
    );
}

#[test]
fn test_bar_re() {
    assert!(
        BAR_RE.is_match(r"volume: 96%   repeat: off   random: off   single: off   consume: on ")
    );
}
