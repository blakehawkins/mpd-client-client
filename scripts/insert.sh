#!/usr/bin/env bash

set -euxo pipefail

if [ -z "$1" ];
then
  exit 1
fi

ret=0
youtube-dl "$1" -v -i -f m4a --restrict-filenames --no-mtime -o '%(epoch)s.%(id)s.%(title).40s.%(ext)s' --default-search "ytsearch" || ret=$?
if [ $ret -ne 0 ] && [ $ret -ne 1 ]; then exit $rc; fi

MPD_HOST="/var/run/mpd/socket" find . -name '*.m4a' -exec bash -c "mv {} /home/pi/media/{}; mpc insert file:///home/pi/media/{}" \;

s=$(df | grep root | tr -s ' ' | cut -d' ' -f5 | cut -d'%' -f1)
len=$(ls /home/pi/media/ -l | wc -l)
while [ $s -gt 75 ] && [ $len -gt 11 ]
do
  ls /home/pi/media -lr | tr -s ' ' | cut -d' ' -f9- | tail -n 1 | xargs -I {} rm /home/pi/media/{}
  s=$(df | grep root | tr -s ' ' | cut -d' ' -f5 | cut -d'%' -f1)
done

echo "The request completed successfully."
