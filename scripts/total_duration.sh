#!/usr/bin/env bash

set -euxo pipefail

./duration.sh | \
    tr ':' ',' | \
    xsv stats | \
    xsv select sum | \
    tr '\n' ' ' | \
    awk -F' ' '{ print $2 + ($3 / 60) + ($4 / 3600) }'

echo "The request completed successfully."
