#!/usr/bin/env bash

set -euxo pipefail

mpc playlist | \
    xargs -I {} ffmpeg -i {} 2>&1 | \
    grep Duration | \
    cut -d',' -f1 | \
    cut -d':' -f2- | \
    cut -d' ' -f2

echo "The request completed successfully."
