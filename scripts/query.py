#!/usr/bin/env python3

import json
import os
import sys

from youtube_search import YoutubeSearch as search

class SuppressPrint:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout

res = None
with SuppressPrint():
    res = search(" ".join(sys.argv).rstrip()).to_dict()

print(json.dumps(res))
