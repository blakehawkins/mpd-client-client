#!/usr/bin/env bash

set -euxo pipefail

if [ -z "$1" ];
then
  exit 1
fi

find /home/pi/media -iname "*$1*" | cut -d '/' -f5 | cut -d'.' -f3- | rev | cut -d'.' -f 2- | rev | head -n 20

echo "The request completed successfully."
