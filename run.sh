#!/usr/bin/env bash

set -euo pipefail

time cargo build > build.out 2>&1
sleep 1
ps -ef | grep http-media-wrapper | tr -s ' ' | cut -d' ' -f2 | xargs kill -9 > stop.out 2>&1 || echo "ok"
sleep 1
./target/debug/http-media-wrapper-rs > http.out 2>&1 &
