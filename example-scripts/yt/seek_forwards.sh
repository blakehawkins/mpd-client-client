#!/usr/bin/env bash

set -euxo pipefail

# ps -ef | grep omxplayer | tr -s ' ' | cut -d' ' -f2 | xargs kill -9
mpc seek +1%

echo "The request completed successfully."
